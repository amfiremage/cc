enum {
	v1 __attribute__((a))
};

enum {
	v2 __attribute__((a)),
	v3
};

enum {
	v4 __attribute__((a)),
	v5 __attribute__((a)) = 42
};
